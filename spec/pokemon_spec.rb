require 'swagger_helper'

describe 'API TEST' do

  path '/pokemons' do
    get 'Retrieves a pokemon' do
        tags 'Pokemons'
        produces 'application/json', 'application/xml'
        parameter name: :page, :in => :path, :type => :integer
  
        response '200', 'name found' do
          schema type: :object,
          properties: {
              anumber: { type: :integer },
              name: { type: :string },
              type1: { type: :string }
              type2: { type: :string }
              total: { type: :integer }
              hp: { type: :integer }
              attack: { type: :integer }
              defense: { type: :integer }
              spatack: { type: :integer }
              spdef: { type: :integer}
              speed: { type: :integer }
              generation: { type: :integer }
              legendary: { type: :boolean }
          },
            required: [ 'page']
  
          let(:id) { Pokemon.create(anumber: '801', name: 'Necrozma', type1: 'Psychic', total: '600', hp: '97', attack: '107', defense: '101',spatack: '127', spdef: '89', speed: '79', generation: '7', legendary: 'True').id }
          run_test!
        end
  
        response '404', 'pokemon not found' do
          let(:id) { 'invalid' }
          run_test!
        end
      end
  end

  path '/pokemons/{id}' do

    get 'Retrieves a pokemon' do
      tags 'Pokemons'
      produces 'application/json', 'application/xml'
      parameter name: :id, :in => :path, :type => :string

      response '200', 'name found' do
        schema type: :object,
        properties: {
            anumber: { type: :integer },
            name: { type: :string },
            type1: { type: :string }
            type2: { type: :string }
            total: { type: :integer }
            hp: { type: :integer }
            attack: { type: :integer }
            defense: { type: :integer }
            spatack: { type: :integer }
            spdef: { type: :integer}
            speed: { type: :integer }
            generation: { type: :integer }
            legendary: { type: :boolean }
        },
          required: [ 'id', 'name' ]

        let(:id) { Pokemon.create(anumber: '801', name: 'Necrozma', type1: 'Psychic', total: '600', hp: '97', attack: '107', defense: '101',spatack: '127', spdef: '89', speed: '79', generation: '7', legendary: 'True').id }
        run_test!
      end

      response '404', 'pokemon not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end