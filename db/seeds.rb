# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

csv_text = File.read(Rails.root.join('db',  'pokemon.csv'))
csv = CSV.parse(csv_text, :headers => true, :encoding => 'ISO-8859-1')
csv.each do |row|
  t = Pokemon.new
    t.anumber = row[0]
    t.name = row[1]
    t.type1 = row[2]
    t.type2 = row[3]
    t.total = row[4]
    t.hp = row[5]
    t.attack = row[6]
    t.defense = row[7]
    t.spatack = row[8]
    t.spdef = row[9]
    t.speed = row[10]
    t.generation = row[11]
    t.legendary = if row[12] == 'False'
                    false
                elsif row[12] == 'True'
                    true
                end
  t.save
  puts "#{t.name}, saved"
end

puts "There are now #{Pokemon.count} rows in the transactions table"